all: main

LD=$(CC)

CFLAGS_INTERNAL=$(CFLAGS)
CFLAGS_INTERNAL+=-Wall -Wextra -Wpedantic -Wstrict-prototypes
LDFLAGS_INTERNAL=$(LDFLAGS)

main: main.o procq.o
	$(LD) $(LDFLAGS_INTERNAL) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS_INTERNAL) $^ -c -o $@

clean:
	rm -rfv *.d *.o main
	rm -rfv compile_commands.json
