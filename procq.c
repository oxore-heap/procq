#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "procq.h"

bool procq_init(struct procq *procq, undertaker_t undertaker)
{
    *procq = (struct procq){
        .bury = undertaker,
    };

    return true;
}

static void procq_list_append(struct procq_list *list, struct procq_proc *proc)
{
    assert(list);
    assert(proc);

    proc->flink = NULL;
    if (list->last) {
        list->last->flink = proc;
    } else {
        list->first = proc;
    }
    list->last = proc;
}

bool procq_add(struct procq *procq, void *context, handler_t handler)
{
    if (context == NULL || handler == NULL)
        return false;

    struct procq_proc *proc = calloc(1, sizeof(struct procq_proc));

    if (proc == NULL)
        return false;

    *proc = (struct procq_proc){
        .flink = NULL,
        .context = context,
        .handler = handler,
    };

    procq_list_append(&procq->operating, proc);

    return true;
}

size_t procq_run(struct procq *procq)
{
    struct procq_proc *current = procq->operating.first;
    struct procq_proc *prev = NULL;
    size_t nfinished = 0;

    while (current != NULL) {
        enum procq_status ret = current->handler(current->context);

        if (ret == PROCQ_RET_FINISHED) {
            struct procq_proc *next = current->flink;

            if (prev != NULL) {
                prev->flink = next;
            } else {
                procq->operating.first = next;
            }
            if (current == procq->operating.last) {
                procq->operating.last = prev;
            }

            if (procq->bury) {
                procq->bury(current->context);
                free(current);
            } else {
                current->flink = NULL;
                procq_list_append(&procq->finished, current);
            }

            nfinished += 1;
            current = next;
        } else {
            prev = current;
            current = current->flink;
        }
    }

    return nfinished;
}

bool procq_kill(struct procq *procq, void *context)
{
    struct procq_proc *current = procq->operating.first;
    struct procq_proc *prev = NULL;

    while (current != NULL) {
        if (current->context == context) {
            if (prev != NULL) {
                prev->flink = current->flink;
            } else {
                procq->operating.first = current->flink;
            }
            if (current == procq->operating.last) {
                procq->operating.last = prev;
            }
            free(current);
            return true;
        }

        prev = current;
        current = current->flink;
    }

    return false;
}

void *procq_getcorpse(struct procq *procq)
{
    struct procq_list *finished = &procq->finished;
    struct procq_proc *proc = finished->first;
    void *context = NULL;

    if (proc) {
        context = proc->context;
        finished->first = finished->first->flink;
        free(proc);
    }

    return context;
}
