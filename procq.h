#include <stdbool.h>
#include <stddef.h>

enum procq_status {
    PROCQ_RET_FINISHED = 0,
    PROCQ_RET_OPERATING,
};

typedef enum procq_status (*handler_t)(void *context);
typedef void (*undertaker_t)(void *corpse);

struct procq_proc {
    struct procq_proc *flink;
    void *context;
    handler_t handler;
};

struct procq {
    undertaker_t bury;
    struct procq_list{
        struct procq_proc *first;
        struct procq_proc *last;
    } operating, finished;
};

bool    procq_init      (struct procq *, undertaker_t);
bool    procq_add       (struct procq *, void *context, handler_t);
size_t  procq_run       (struct procq *);
bool    procq_kill      (struct procq *, void *context);
void   *procq_getcorpse (struct procq *);
