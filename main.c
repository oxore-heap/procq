#include <assert.h>
#include <stdio.h>

#include "procq.h"

static enum procq_status two_step_handler(void *context)
{
    printf("hello from two_step_handler\n");
    if (*(int *)context < 100) {
        *(int *)context = 100;
        return PROCQ_RET_OPERATING;
    }
    return PROCQ_RET_FINISHED;
}

static enum procq_status one_step_handler(void *context)
{
    printf("hello from one_step_handler\n");
    *(int *)context = 100;
    return PROCQ_RET_FINISHED;
}

static void test_just_one_process(void)
{
    struct procq procq = {0};
    int context = 0;

    assert(procq_init(&procq, NULL));
    assert(procq_add(&procq, &context, &one_step_handler));
    assert(1 == procq_run(&procq));
    assert(0 == procq_run(&procq));
    assert(&context == procq_getcorpse(&procq));
    assert(NULL == procq_getcorpse(&procq));
    assert(context == 100);
}

static void test_two_processes_run_one_kill_another(void)
{
    struct procq procq = {0};
    int context1 = 0;
    int context2 = 0;

    assert(procq_init(&procq, NULL));

    /* Add both with two step handlers */

    assert(procq_add(&procq, &context1, &two_step_handler));
    assert(procq_add(&procq, &context2, &two_step_handler));

    /* Run both one step */

    assert(0 == procq_run(&procq));

    /* Kill first. Note: if it killed then no corpse remaining */

    assert(true == procq_kill(&procq, &context1));

    /* Run second - it finishes - and then get it's corpse */

    assert(1 == procq_run(&procq));
    assert(&context2 == procq_getcorpse(&procq));

    /* No running or finished processes remaining */

    assert(NULL == procq_getcorpse(&procq));

    assert(context1 == 100);
    assert(context2 == 100);
}

static void undertaker(void *corpse)
{
    *(int *)corpse = 0xDEAD;
}

static void test_two_processes_autobury(void)
{
    struct procq procq = {0};
    int context1 = 0;
    int context2 = 0;

    assert(procq_init(&procq, &undertaker));

    /* Add both */

    assert(procq_add(&procq, &context1, &one_step_handler));
    assert(procq_add(&procq, &context2, &two_step_handler));

    /* Run both two step */

    assert(1 == procq_run(&procq));
    assert(1 == procq_run(&procq));

    /* No running or finished processes remaining */

    assert(NULL == procq_getcorpse(&procq));

    assert(context1 == 0xDEAD);
    assert(context2 == 0xDEAD);
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test_just_one_process();
    test_two_processes_run_one_kill_another();
    test_two_processes_autobury();
}
